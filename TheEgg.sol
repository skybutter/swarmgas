// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";



contract TheEgg is ERC721, ERC721Enumerable, Ownable {
    using Strings for uint256;
    using SafeERC20 for IERC20;

    address public constant POOL = 0x8bDc87BF6a9625205bCBeC270448f3C7bF4e00c3;

    // WhiteLists for presale.
    mapping (address => uint) public _numberOfPresale;
    mapping (uint => uint) public _numberOfAction;

    uint256 public ETH_PRICE = 10000000000000000; // 0.01 ETH
    uint256 public FOOD_PRICE = 10000000000000000000000000000;
    uint256 public FOOD_FEED = 2000000000000000000000000000;
    uint256 public MAX_TOKENS = 10000;
    bool public saleIsActive = false;
    string public baseExtension = ".json";
    string private _baseURIextended;

    IERC20 public FOOD;
    
    constructor(
        address _food
    ) ERC721("TheEgg", "EGG") {
        
        FOOD = IERC20(_food);
        _numberOfPresale[0x84CCf38452Dc6bB59DBccD5E1BA465f7BF2e66a7] = 2;

    }

    function _beforeTokenTransfer(address from, address to, uint256 tokenId) internal override(ERC721, ERC721Enumerable) {
        super._beforeTokenTransfer(from, to, tokenId);
    }

    function supportsInterface(bytes4 interfaceId) public view virtual override(ERC721, ERC721Enumerable) returns (bool) {
        return super.supportsInterface(interfaceId);
    }

    function setBaseURI(string memory baseURI_) external onlyOwner() {
        _baseURIextended = baseURI_;
    }

    function _baseURI() internal view virtual override returns (string memory) {
        return _baseURIextended;
    }


    function tokenURI(uint256 tokenId) public view virtual override returns (string memory)
    {
        require(_exists(tokenId), "ERC721Metadata: URI query for nonexistent token");

        string[6] memory action_list = ['a', 'b', 'c', 'd', 'e', 'f'];
    
        string memory currentBaseURI = _baseURI();
        return bytes(currentBaseURI).length > 0
            ? string(abi.encodePacked(currentBaseURI, tokenId.toString(), action_list[_numberOfAction[tokenId]], baseExtension))
            : "";
    }

    function flipSaleState() public onlyOwner {
        saleIsActive = !saleIsActive;
    }


    function preSaleToken(uint numberOfTokens) public {
        require(!saleIsActive, "Could not pre-mint after sales active");
        require(numberOfTokens <= _numberOfPresale[msg.sender], "Exceeded max token purchase for pre-sale ");
        require(totalSupply() + numberOfTokens <= MAX_TOKENS, "Purchase would exceed max supply of tokens");

        for(uint i = 0; i < numberOfTokens; i++) {
            uint mintIndex = totalSupply();
            if (totalSupply() < MAX_TOKENS) {
                _safeMint(msg.sender, mintIndex);
            }
        }
        
        _numberOfPresale[msg.sender] = _numberOfPresale[msg.sender] - numberOfTokens;
    }
    
    function mintWithEthToken(uint numberOfTokens) public payable {
        require(saleIsActive, "Sale must be active to mint Tokens");
        require(totalSupply() + numberOfTokens <= MAX_TOKENS, "Purchase would exceed max supply of tokens");
        require(ETH_PRICE * numberOfTokens <= msg.value, "Ether value sent is not correct");

        for(uint i = 0; i < numberOfTokens; i++) {
            uint mintIndex = totalSupply();
            if (totalSupply() < MAX_TOKENS) {
                _safeMint(msg.sender, mintIndex);
            }
        }
    }

    function mintWithFoodToken(uint numberOfTokens) public {
        require(tx.origin == msg.sender, "You dont own");
        require(saleIsActive, "Sale must be active to mint Tokens");
        require(totalSupply() + numberOfTokens <= MAX_TOKENS, "Purchase would exceed max supply of tokens");

        FOOD.safeTransferFrom(msg.sender, address(this), numberOfTokens * FOOD_PRICE);

        for(uint i = 0; i < numberOfTokens; i++) {
            uint mintIndex = totalSupply();
            if (totalSupply() < MAX_TOKENS) {
                _safeMint(msg.sender, mintIndex);
            }
        }
    }

    function Feeding(uint tokenId) public {
        require(tx.origin == msg.sender, "You dont own");
        require(ownerOf(tokenId) == msg.sender, "You are not token's owner");
        require(_numberOfAction[tokenId] <= 5, "Exceed max actions of token");
        require(FOOD.balanceOf(msg.sender) >= FOOD_FEED, "FOOD token is not enough");

        FOOD.transferFrom(msg.sender, POOL, FOOD_FEED);
        
        _numberOfAction[tokenId]++;

    }

    // Update ACTION Food
    function setFoodFeed(uint _newPrice) external onlyOwner {
        FOOD_FEED = _newPrice;
    }


    // Update ETH Price
    function setEthPrice(uint _newPrice) external onlyOwner {
        ETH_PRICE = _newPrice;
    }

    // Update FOOD Price
    function setFoodPrice(uint _newPrice) external onlyOwner {
        FOOD_PRICE = _newPrice;
    }

    function addWhiteList(address _wallet, uint _count) external onlyOwner {
        _numberOfPresale[_wallet] = _count;
    }

    function withdraw() public onlyOwner {
        uint256 balance = address(this).balance;
        payable(msg.sender).transfer(balance);
    }

}